apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  creationTimestamp: null
  name: nexus.nexus.webservices.cern.ch
spec:
  group: nexus.webservices.cern.ch
  names:
    kind: Nexus
    listKind: NexusList
    plural: nexus
    singular: nexus
  scope: Namespaced
  versions:
  - name: v1alpha1
    schema:
      openAPIV3Schema:
        description: Nexus is the Schema for the nexus API
        properties:
          apiVersion:
            description: 'APIVersion defines the versioned schema of this representation
              of an object. Servers should convert recognized schemas to the latest
              internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources'
            type: string
          kind:
            description: 'Kind is a string value representing the REST resource this
              object represents. Servers may infer this from the endpoint the client
              submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds'
            type: string
          metadata:
            type: object
          spec:
            description: Spec defines the desired state of the Nexus instance
            properties:
              cern-auth-proxy:
                description: Configures how Nexus is exposed to clients.
                properties:
                  route:
                    properties:
                      hostname:
                        description: |
                          The desired hostname of the Nexus instance. Should be in the web.cern.ch
                          subdomain. Examples: nexus.web.cern.ch, myrepomanager.web.cern.ch.
                        format: hostname
                        type: string
                      public:
                        default: false
                        description: Visibility outside the CERN network. False by
                          default for Nexus since the most common scenario is anonymous
                          access from CERN network.
                        type: boolean
                    required:
                    - hostname
                    type: object
                required:
                - route
                type: object
              initialNexusConfiguration:
                description: |
                  Settings for the initial Nexus configuration. These settings are only used at instance creation; any changes after the initial provisioning are ignored.
                properties:
                  initialAdminEgroup:
                    description: A single e-group that is granted the initial admin
                      access to the Nexus instance. Only this e-group will have permissions
                      to do anything in Nexus initially; members of this group can
                      then use the Nexus web UI to grant appropriate privileges to
                      other groups.
                    minLength: 1
                    type: string
                required:
                - initialAdminEgroup
                type: object
              nexus-repository-manager:
                description: |
                  Configuration for Nexus deployment and storage.
                properties:
                  persistence:
                    description: Configures Nexus persistent storage
                    properties:
                      storageSize:
                        default: 10Gi
                        description: Size of the Nexus storage (hosts Nexus database
                          and blobs). Can be extended as needed as long as there is
                          sufficient storage quota in the project.
                        pattern: ^(\d+(e\d+)?|\d+(\.\d+)?(e\d+)?[EPTGMK]i?)$
                        type: string
                    type: object
                type: object
            required:
            - initialNexusConfiguration
            - cern-auth-proxy
            type: object
          status:
            description: Status defines the observed state of Grafana
            properties:
              conditions:
                description: Conditions represent the latest available observations
                  of an object's state
                items:
                  description: Condition contains details for one aspect of the current
                    state of this API Resource.
                  properties:
                    lastTransitionTime:
                      description: lastTransitionTime is the last time the condition
                        transitioned from one status to another. This should be when
                        the underlying condition changed.  If that is not known, then
                        using the time when the API field changed is acceptable.
                      format: date-time
                      type: string
                    message:
                      description: message is a human readable message indicating
                        details about the transition. This may be an empty string.
                      maxLength: 32768
                      type: string
                    reason:
                      description: reason contains a programmatic identifier indicating
                        the reason for the condition's last transition. Producers
                        of specific condition types may define expected values and
                        meanings for this field, and whether the values are considered
                        a guaranteed API. The value should be a CamelCase string.
                        This field may not be empty.
                      maxLength: 1024
                      minLength: 1
                      pattern: ^[A-Za-z]([A-Za-z0-9_,:]*[A-Za-z0-9_])?$
                      type: string
                    status:
                      description: status of the condition, one of True, False, Unknown.
                      enum:
                      - "True"
                      - "False"
                      - Unknown
                      type: string
                    type:
                      description: type of condition in CamelCase or in foo.example.com/CamelCase.
                        --- Many .condition.type values are consistent across resources
                        like Available, but because arbitrary conditions can be useful
                        (see .node.status.conditions), the ability to deconflict is
                        important. The regex it matches is (dns1123SubdomainFmt/)?(qualifiedNameFmt)
                      maxLength: 316
                      pattern: ^([a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*/)?(([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9])$
                      type: string
                  required:
                  - status
                  - type
                  type: object
                type: array
              deployedRelease:
                description: DeployedRelease contains information about the Helm Release
                  deployed for this API Resource.
                properties:
                  manifest:
                    description: The resources deployed by the Helm Release
                    type: string
                  name:
                    description: Name of the Helm Release
                    type: string
                type: object
            required:
            - conditions
            type: object
        type: object
    served: true
    storage: true
    subresources:
      status: {}
status:
  acceptedNames:
    kind: ""
    plural: ""
  conditions: null
  storedVersions: null
